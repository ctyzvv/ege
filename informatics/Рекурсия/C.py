import sys

sys.setrecursionlimit(999999999)
n = int(input())


def myFunc(n, a=2):
    if a * a < n:
        if n > a and n % a != 0:
            return myFunc(n, a + 1)
        else:
            return False
    return True


if myFunc(n):
    print("YES")
else:
    print("NO")
