
arr = []

k = 1
maxValue = -99999

while k != 0:
    k = int(input())
    if k != 0:
        maxValue = max(k, maxValue)
        arr.append(k)

print(arr.count(maxValue))