n = int(input())

res = ''


def myFunc(n):
    global res
    if n:
        d = n % 3
        if d == 2:
            res += '#'
            n += 1
        else:
            res += str(d)
        n //= 3
        myFunc(n)


myFunc(n)
if n == 0:
    print('0')
else:
    print(res[::-1])
