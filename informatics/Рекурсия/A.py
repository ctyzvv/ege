s = int(input())
ch = [0, 2, 4, 6, 8]

count = 0


def myFunc(n):
    if n != 0:
        if n % 10 in ch:
            global count
            count += 1
        n //= 10
        myFunc(n)
    else:
        pass


myFunc(s)

print(count)
