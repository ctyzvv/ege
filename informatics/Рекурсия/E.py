n = str(input())

res = ''
if n[0] == '-':
    res = '-'
    n = n[1:]

nReverse = n[::-1]
parts = []
while nReverse:
    value = nReverse[0:4]
    if len(value) < 4:
        value = (4 - len(value)) * '0' + value[::-1]
    else:
        value = value[::-1]
    parts.append(value)
    nReverse = nReverse[4:]
parts.reverse()

symbols = {
    '0': '0',
    '1': '1',
    '2': '2',
    '3': '3',
    '4': '4',
    '5': '5',
    '6': '6',
    '7': '7',
    '8': '8',
    '9': '9',
    '10': 'A',
    '11': 'B',
    '12': 'C',
    '13': 'D',
    '14': 'E',
    '15': 'F'
}

for i in parts:
    sum = 0
    sum += int(i[3]) * 1
    sum += int(i[2]) * 2
    sum += int(i[1]) * 4
    sum += int(i[0]) * 8
    res += symbols[str(sum)]
print(res)
