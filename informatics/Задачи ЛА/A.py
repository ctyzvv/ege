n, k = map(int, input().split())
arr = list(map(int, input().split()))

maxSum = -999999
maxVal = -999999
l = 0
r = 0
leftArr = []
for i in range(k + 1, n):
    leftArr.append(arr[i - k - 1])
    maxVal = max(arr[i-k-1], maxVal)

    if maxVal + arr[i] > maxSum:
        l = leftArr.index(maxVal)
        r = i
        maxSum = maxVal + arr[i]

l += 1
r += 1
print(l, r)