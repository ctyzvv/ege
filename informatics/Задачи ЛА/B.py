n, x = map(int, input().split())
arr1 = list(map(int, input().split()))
arr2 = list(map(int, input().split()))

l = -2
r = -2

maxProfit = 0

money = 0

minIndex = 0
maxIndex = 0

minBuy = 99999999999
maxSell = -99999999999

buy = []

for i in range(0, n):
    
    if arr2[i] - minBuy > maxProfit:
        maxProfit = arr2[i] - minBuy
        l = minIndex
        r = i

    if minBuy > arr1[i] and i != (n - 1):
        minBuy = arr1[i]
        minIndex = i

if l != -2:
    count = x // arr1[l]
    money = count * arr2[r] - count * arr1[l]

l += 1
r += 1

print(x + money)
print(l, r)
