n = int(input())
a = list(map(int, input().split()))
count = a.count(0)
s = ''
if count > 0:
    s = '0'
print(*(list(filter(lambda item: item != 0, a))), end=' ')
print((count-1) * '0 ', s, sep='')