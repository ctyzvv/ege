try:
    k, n = map(int, input().split())
    a = list(map(int, input().split()))


    def search(l, r):
        mid = (l + r) // 2
        value = a[mid]

        if l == r - 1:
            if a[l] == k:
                return l + 1
            elif a[r] == k:
                return r + 1
            else:
                return 0

        if value < k:
            return search(mid, r)
        elif value >= k:
            return search(l, mid)


    print(search(0, n - 1))
except:
    print(0)