x1, y1, x2, y2, x3, y3 = map(int, input().split())

s1 = ((x2 - x1)**2 + (y2-y1)**2)**0.5
s2 = ((x3 - x1)**2 + (y3-y1)**2)**0.5
s3 = ((x2 - x3)**2 + (y2-y3)**2)**0.5

print('{:.6f}'.format(s1 + s2 + s3))