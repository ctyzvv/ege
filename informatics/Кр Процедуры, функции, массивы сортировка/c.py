l, r = map(int, input().split())

for i in range(l, r + 1):
    print(i**2, end=' ')