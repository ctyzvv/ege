n = int(input())
a = list(map(int, input().split()))

maxValue = max(a)
minValue = min(a)

maxIndex = a.index(maxValue)
minIndex = a.index(minValue)

a[maxIndex], a[minIndex] = a[minIndex], a[maxIndex]

print(*a)