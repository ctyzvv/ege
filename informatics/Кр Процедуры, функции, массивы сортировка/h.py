n = int(input())
a = list(map(int, input().split()))
a.sort()
res = 0
value = 1
index = -1
count = 1

for i in range(len(a)):
    if a[i] >= n:
        value = a[i]
        count = 1
        last = i
        for j in range(i + 1, len(a)):
            if a[j] - a[last] >= 3:
                count += 1
                last = j
        if count > res: res = count

print(res)
