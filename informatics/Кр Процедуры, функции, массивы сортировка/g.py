a = list(map(int, input().split()))


def selectionSort(list):
    for i in range(len(list)):
        start = i
        for j in range(i + 1, len(list)):
            if list[start] < list[j]:
                start = j

        list[i], list[start] = list[start], list[i]

    return list


print(*(selectionSort(a)))
