n, k = map(int, input().split())

arr1 = list(map(int, input().split()))
arr2 = list(map(int, input().split()))


for i in arr2:
    found = False
    l = 0
    r = n
    while l < r - 1:
        mid = (l + r) // 2
        if i < arr1[mid]:
            r = mid
        else:
            l = mid
    if arr1[l] == i:
        print('YES')
    else:
        print('NO')
