n = int(input())
arr = list(map(int, input().split()))

value = arr[0]
count = 1

ansValue = arr[0]
ansCount = 1
for i in range(1, n):
    if value == arr[i]:
        count += 1
        if count > ansCount:
            ansValue = value
            ansCount = count
    else:
        value = arr[i]
        count = 1

print(ansValue, ansCount)
