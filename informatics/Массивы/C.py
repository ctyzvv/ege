from random import randrange, randint

a, b, n = map(int, input().split())

res = []

for _ in range(n):
    res.append(randint(a, b))

sum1 = 0
count1 = 0
sum2 = 0
count2 = 0
for i in res:
    if i < 50:
        sum1 += i
        count1 += 1
    else:
        sum2 += i
        count2 += 1

print(*res)
try:
    avg1 = sum1 / count1
except:
    avg1 = 0
try:
    avg2 = sum2 / count2
except:
    avg2 = 0
print('{:.3f} {:.3f}'.format(avg1, avg2))
