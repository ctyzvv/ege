n = int(input())
arr = list(map(int, input().split()))

res = set()

arr.sort()

for i in range(len(arr)):
    el = arr[i]
    if not el in res:
        for j in range(i+1, len(arr)):
            if arr[j] == arr[i]:
                res.add(el)

if len(res):
    print(*res)
else:
    print(0)