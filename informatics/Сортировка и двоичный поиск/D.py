n = int(input())
arr = list(map(int, input().split()))


mid = n // 2

for i in range(mid - 1):
    for j in range(mid - i - 1):
        if arr[j] > arr[j+1]:
            arr[j], arr[j+1] = arr[j+1], arr[j]

for i in range(mid, n - 1,  1):
    for j in range(mid, n - 1, 1):
        if arr[j] < arr[j+1]:
            arr[j], arr[j+1] = arr[j+1], arr[j]

print(*arr)