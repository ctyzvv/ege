n = int(input())

count = 0
m = 1
while m < n:
    m *= 2
    count += 1

print(count)