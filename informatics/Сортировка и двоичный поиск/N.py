import copy
n, w = map(int, input().split())
l, r = map(int, input().split())
a = list(map(int, input().split()))

b = copy.copy(a)

a.sort()

ans = 0
if w > r - l:
    print(-1)
else:
    minEl = a[0]
    maxEl = a[-1]

    remove = 0

    if minEl - l > r - maxEl:
        count = minEl - l
        remove = w - count
        a = a[n - remove:]
    else:
        count = r - maxEl
        remove = w - count
        a = a[0:remove]

    print(remove)
    for i in a:
        print(b.index(i) + 1)


"""
1 6 
1 3
1

"""
