n = int(input())
arr = list(map(int, input().split()))

started = False

for i in range(n - 1):
    found = False
    for j in range(n - 2, i - 1, -1):
        if arr[j + 1] < arr[j]:
            arr[j], arr[j + 1] = arr[j + 1], arr[j]
            found = True
            started = True
    if not found:
        break
    else:
        print(*arr)

if not started: print(*arr)