n = int(input())
arr = list(map(int, input().split()))

for i in range(n-1):
    for j in range(n-2, i-1, -1):

        sum1 = 0
        for k in str(arr[j + 1]):
            sum1 += int(k)

        sum2 = 0
        for k in str(arr[j]):
            sum2 += int(k)

        if sum1 > sum2:
            arr[j], arr[j+1] = arr[j+1], arr[j]

print(*arr)