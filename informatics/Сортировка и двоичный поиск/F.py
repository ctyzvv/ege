n = int(input())
arr = list(map(int, input().split()))
k, m, d = map(int, input().split())

k -= 1
m -= 1

for i in range(k, m + 1):
    for j in range(k, m):
        if d == -1:
            if arr[j] < arr[j+1]:
                arr[j], arr[j+1] = arr[j+1], arr[j]
        else:
            if arr[j] > arr[j + 1]:
                arr[j], arr[j + 1] = arr[j + 1], arr[j]

print(*arr)
