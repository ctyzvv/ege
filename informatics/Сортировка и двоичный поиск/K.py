n, k = map(int, input().split())
arr = list(map(int, input().split()))

l = 0
r = arr[-1]

while l < r - 1:
    mid = (l + r) // 2
    last = arr[0]
    c = 1
    for i in arr:
        if i - last >= mid:
            c += 1
            last = i

    if c >= k:
        l = mid
    else:
        r = mid

print(l)
