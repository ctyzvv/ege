x, y = map(float, input().split())
count = 1
while y - x > 0.000001 and x < y:
    x *= 1.7
    count += 1
print(count)
