from math import pi
n = int(input())
print('{:.3f}\n{:.3f}'.format(n**2 * pi, pi * 2*n))
