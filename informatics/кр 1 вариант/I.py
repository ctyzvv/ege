m, n = map(int, input().split())
x, y = map(int, input().split())

# left
if x > 1:
    print(x - 1, y)
# top
if y != n:
    print(x, y + 1)
# right
if x != m:
    print(x + 1, y)
# bottom
if y > 1:
    print(x, y - 1)