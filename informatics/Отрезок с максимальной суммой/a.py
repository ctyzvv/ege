n, k, m = map(int, input().split())
a = list(map(int, input().split()))

p = [0] * (n+1)
for i in range(1, n + 1):
    p[i] = p[i-1] + a[i - 1]


ans = 0

for i in range(k, n + 1):
    if p[i] - p[i-k - 1] == m:
        ans = i - k
        break

print(int(ans))

""""

5 1 22
1 9 13 10 -11

"""