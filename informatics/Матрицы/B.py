n, m = map(int, input().split())

arr = []

sum1 = 0

for i in range(n):
    inp = list(map(int, input().split()))
    sum1 += sum(inp)
    arr.append(inp)

sum1 /= n * m

print('{:.4f}'.format(sum1))
for i in range(n):
    for j in range(m):
        if (arr[i][j] < sum1):
            print('{:5d}'.format(0), end="")
        else:
            print('{:5d}'.format(255), end="")
    print()
