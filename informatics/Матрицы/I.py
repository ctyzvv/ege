n, m = map(int, input().split())
matrix = []

for i in range(n):
    arr = list(map(int, input().split()))
    matrix.append(arr)

for i in range(m):
    newArr = []
    for j in range(n):
        newArr.append(matrix[j][i])
    newArr = newArr[::-1]

    for j in range(n):
        matrix[j][i] = newArr[j]





for i in range(n):
    for j in range(m):
        print('{:3d}'.format(matrix[i][j]), end=" ")
    print()


"""
4 5
11 12 13 14 15
16 17 18 19 20
21 22 23 24 25
26 27 28 29 30
"""