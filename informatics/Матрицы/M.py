n, m = map(int, input().split())
matrix = []
# n = 3
# m = 5
# Tests
# ===========================
# matrix = [
#     [0, -1, 0, 0, 0],
#     [0, -1, 0, -1, 0],
#     [0, 0, 0, -1, 0],
#     [-1, -1, -1, -1, 0],
#     [0, 0, -1, 0, 0],
# ]


# matrix = [
#     [0, 0, 0],
#     [-1, -1, 0]
# ]

# matrix = [
#     [0, -1, 0, 0, 0],
#     [0, -1, 0, -1, 0],
#     [0, 0, 0, -1, 0],
#
# ]

# matrix = [
#     [0, 0, 0],
#     [-1, -1, 0],
#     [0, 0, 0],
#     [0, -1, -1],
#     [0, 0, 0]
# ]
# ===========================


for i in range(n):
    arr = list(map(int, input().split()))
    matrix.append(arr)

positionX = 0
positionY = 0
dots = [[0, 0, 1]]

matrix[0][0] = 1

for i in dots:
    dotY = i[0]
    dotX = i[1]
    dotValue = i[2]
    used = False
    # Right
    if dotX + 1 <= m - 1 and matrix[dotY][dotX + 1] != -1:
        if dotValue < matrix[dotY][dotX + 1] or matrix[dotY][dotX + 1] == 0:
            matrix[dotY][dotX + 1] = dotValue + 1
            dots.append([dotY, dotX + 1, dotValue + 1])

    # Down
    if dotY + 1 <= n - 1 and matrix[dotY + 1][dotX] != -1:
        if dotValue < matrix[dotY + 1][dotX] or matrix[dotY + 1][dotX] == 0:
            matrix[dotY + 1][dotX] = dotValue + 1
            dots.append([dotY + 1, dotX, dotValue + 1])

    # Top
    if dotY - 1 >= 0 and matrix[dotY - 1][dotX] != -1:
        if dotValue < matrix[dotY - 1][dotX] or matrix[dotY - 1][dotX] == 0:
            matrix[dotY - 1][dotX] = dotValue + 1
            dots.append([dotY - 1, dotX, dotValue + 1])
            used = True

    # Left
    if dotX - 1 >= 0 and matrix[dotY][dotX - 1] != -1:
        if dotValue < matrix[dotY][dotX - 1] or matrix[dotY][dotX - 1] == 0:
            matrix[dotY][dotX - 1] = dotValue + 1
            dots.append([dotY, dotX - 1, dotValue + 1])
            used = True


if matrix[n - 1][m - 1] != 0:
    print(matrix[n - 1][m - 1] - 1)
else:
    print(-1)

for i in range(n):
    for j in range(m):
        print('{:3d}'.format(matrix[i][j]), end=" ")
    print()
