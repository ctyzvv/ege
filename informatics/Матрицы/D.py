n, m = map(int, input().split())

matrix = []
for i in range(n):
    arr = []
    for _ in range(m):
        arr.append(0)
    matrix.append(arr)

value = 1
column = 1
for i in range(m):
    if column % 2 != 0:
        for j in range(n):
            matrix[j][i] = value
            value += 1
    else:
        for j in range(n-1, -1, -1):
            matrix[j][i] = value
            value += 1

    column += 1


for i in range(n):
    for j in range(m):
        print('{:3d}'.format(matrix[i][j]), end=" ")
    print()
