n, m = map(int, input().split())

matrix = []
for i in range(n):
    arr = []
    for _ in range(m):
        arr.append(0)
    matrix.append(arr)


matrix[0][0] = 1
value = 2

positionX = 0
positionY = 0

b = True
while value < n * m:
    if not b:
        while positionX < m - 1 and positionY > 0:
            positionX += 1
            positionY -= 1
            matrix[positionY][positionX] = value
            value += 1
            b = True
    if not b:
        while 0 < positionX and positionY < n - 1:
            positionX -= 1
            positionY += 1
            matrix[positionY][positionX] = value
            value += 1
            b = True

    if b:
        if (positionX == 0 or positionX == m - 1) and positionY < n - 1:
            positionY += 1
            matrix[positionY][positionX] = value
            value += 1
        else:
            positionX += 1
            matrix[positionY][positionX] = value
            value += 1
        b = False


for i in range(n):
    for j in range(m):
        print('{:3d}'.format(matrix[i][j]), end=" ")
    print()
