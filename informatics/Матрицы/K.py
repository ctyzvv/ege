n = int(input())

matrix = []

for i in range(n):
    arr = []
    for _ in range(n):
        arr.append(0)
    matrix.append(arr)

for i in range(n-1, -1, -1):
    arr = list(map(int, input().split()))
    for j in range(n):
        matrix[j][i] = arr[j]

for i in range(n):
    for j in range(n):
        print('{:3d}'.format(matrix[i][j]), end=" ")
    print()
