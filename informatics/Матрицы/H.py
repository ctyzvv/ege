n, m = map(int, input().split())
matrix = []

for i in range(n):
    arr = list(map(int, input().split()))
    matrix.append(arr[::-1])


for i in range(n):
    for j in range(m):
        print('{:3d}'.format(matrix[i][j]), end=" ")
    print()
