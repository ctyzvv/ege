n = int(input())

matrix = []

for i in range(n):
    arr = []
    for _ in range(n):
        arr.append(0)
    matrix.append(arr)

for i in range(n):
    arr = list(map(int, input().split()))
    el = n - 1
    for j in range(n):
        matrix[j][i] = arr[el]
        el -= 1

for i in range(n):
    for j in range(n):
        print('{:3d}'.format(matrix[i][j]), end=" ")
    print()
