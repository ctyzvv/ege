n = int(input())
matrix = []

for i in range(n):
    arr = list(map(int, input().split()))
    matrix.append(arr)

for i in range(n):
    arr = matrix[i]
    newArr = []
    for j in range(0, i+1, 1):
        newArr.append(arr[j])
    for _ in range(n, i, -1):
        newArr.append(0)

    matrix[i] = newArr


for i in range(n):
    for j in range(n):
        print('{:3d}'.format(matrix[i][j]), end=" ")
    print()

"""


5
1 2 3 4 5
6 7 8 9 1
5 4 3 2 1
1 9 8 7 6
4 5 6 7 8


"""
