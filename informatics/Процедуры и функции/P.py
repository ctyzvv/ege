n = int(input())


def checkSimple(n):
    for i in range(2, int(n ** 0.5) + 1):
        if n % i == 0:
            return False
    return n != 1


simple = True
while n > 0:
    if checkSimple(n):
        n //= 10
    else:
        simple = False
        break

if simple:
    print('YES')
else:
    print('NO')
