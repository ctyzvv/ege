a, b = map(int, input().split())


# Euclid's Algorithm
def getNOD(a, b):
    while a != 0 and b != 0:
        if a > b:
            a = a % b
        else:
            b = b % a

    return a + b


def getNOK(a, b):
    return int(a * b / getNOD(a, b))


print(getNOK(a, b))
