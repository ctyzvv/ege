a, b = map(int, input().split())

k = 2
sumA = {1}
sumB = {1}
while k < int(max(a, b)**0.5 + 1):
    if a % k == 0:
        sumA.add(k)
        sumA.add(a / k)
    if b % k == 0:
        sumB.add(k)
        sumB.add(b / k)
    k += 1

if sum(sumA) == b and sum(sumB) == a:
    print('YES')
else:
    print('NO')