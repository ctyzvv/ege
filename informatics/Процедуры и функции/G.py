n, b = map(int, input().split())

alphabet = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ"


def myFunc(n, b):
    value = ''
    if n < 0:
        value += '-'
    res = ''
    n = abs(n)

    while n > 0:
        res += alphabet[n % b]
        n //= b
    if res == '':
        res = '0'
    return value + res[::-1]


print(myFunc(n, b))
