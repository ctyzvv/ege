w, h, n = map(int, input().split())

l = 0
r = max(w, h) * n


while l < r - 1:
    mid = (l + r) // 2
    count = (mid // w) * (mid // h)

    if count >= n:
        r = mid
    else:
        l = mid

print(r)

