n, x, y = map(int, input().split())

l = 0
r = n * max(x, y) + 1

time = min(x, y)


def check(val):
    return val // x + val // y < n - 1


while l < r - 1:
    mid = (l + r) // 2
    if check(mid):
        l = mid
    else:
        r = mid

if n == 1:
    print(time)
else:
    print(time + r)
