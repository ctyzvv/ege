l, r = map(int, input().split())

res = []

s = ''

found = []


def getDividers(n):
    res = {1, }
    for i in range(2, int(n ** 0.5) + 1):
        if n % i == 0:
            res.add(i)
            res.add(n // i)
    return sum(res)


for i in range(l, r + 1):
    res.append(getDividers(i))

for i in range(len(res)):

    if l <= res[i] <= r:
        second = res[res[i] - l]
        if second == i + l and second < res[i]:
            found.append([res[i], second])

if len(found) != 0:
    for i in range(len(found)):
        if i != len(found) - 1:
            print('({},{})'.format(found[i][1], found[i][0]), end=' ')
        else:
            print('({},{})'.format(found[i][1], found[i][0]))
else:
    print(0)

