n = int(input())

arr = [1, 1]

for i in range(2, n):
    arr.append(arr[i-1] + arr[i-2])


if n == 1:
    print(1)
else:
    print(*arr)
