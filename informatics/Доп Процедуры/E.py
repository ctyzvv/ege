a = int(input())


def getDivides(a):
    sum = 1
    for i in range(2, int(a ** 0.5) + 1):
        if a % i == 0:
            sum += i
            sum += a // i
    return sum


if getDivides(a) == a:
    print('YES')
else:
    print('NO')
