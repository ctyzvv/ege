n = int(input())

data = {

}

upperCaseLetters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'

for i in range(n):
    s = str(input())
    lowerCaseS = s.lower()
    for j in range(len(s)):
        if s[j] in upperCaseLetters:
            if lowerCaseS in data:
                data[lowerCaseS].append(j)
            else:
                data[lowerCaseS] = [j]

line = input().split()

mistakes = 0

for word in line:
    lowerCaseWord = word.lower()
    upperCaseWord = word.upper()
    accent = 0
    if upperCaseWord == word or lowerCaseWord == word:
        mistakes += 1
        continue
    else:
        for j in range(len(word)):
            if word[j] in upperCaseLetters:
                accent += 1
                if lowerCaseWord in data:
                    dictionary = data[lowerCaseWord]

                    if (not (j in dictionary)) or accent > 1:
                        mistakes += 1
                        break

                elif accent > 1:
                    mistakes += 1
                    break

print(mistakes)


"""
TESTS

1
cAnnot
cannot

1
===========


2
cAnnot
cannOt
cAnnot cannOt

2
===========

1
cAnnot
The

0
===========

1
cAnnot
ThE

1
==========

1
cAnnot
CANNOT

1
=========

1
cannOt
cannot CANNOT CANNOT CANNOT

4
=========


"""

