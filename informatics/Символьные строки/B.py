s = str(input())

res = ''
count = 0

for i in s:
    if i == 'a':
        res += 'b'
        count += 1
    elif i == 'A':
        res += 'B'
        count += 1
    elif i == 'b':
        res += 'a'
        count += 1
    elif i == 'B':
        res += 'A'
        count += 1
    else:
        res += i

print(res)
print(count)