from itertools import product
cmb = list(product("ДЖОБС", repeat=6))

answer = 0

def is_match(word):
    if word.count("Ж") > 2:
        return 0
    if word.count("Д") != 1 or word.count("О") != 1 or word.count("С") != 1:
        return 0
    return 1

for i in cmb:
    word = ""
    for j in i:
        word += j

    if is_match(word):
        answer += 1

print("№177:", answer)
