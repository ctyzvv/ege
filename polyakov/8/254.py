numbers = "0123456789ABCDEF"
def toBase(n, base):
    result = ""
    while n:
        result = str(numbers[n%base]) + result
        n //= base
    return result

def is_match(word):
    if not (word[0] == "F" and word[-1] == "A"):
        return 0
    if word.count("3B") != 1:
        return 0
    return 1

answer = 0

for i in range(int("F0000", 16), int("FFFFF", 16)):
    if is_match(toBase(i, 16)):
        answer += 1

print("№254:", answer)
