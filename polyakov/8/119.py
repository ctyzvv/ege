from itertools import product
cmb = list(product("МАГИСТР", repeat=5))

answer = 0

def is_match(word):
    if word.count("А") + word.count("И") > 1:
        return 0

    for i in word:
        if word.count(i) > 1:
            return 0

    return 1
    

for i in cmb:
    word = ""
    for j in i:
        word += j

    if is_match(word):
        answer += 1
    
print("№119:", answer)
