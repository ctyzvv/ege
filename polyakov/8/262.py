def toBase(n, base):
    result = ""
    while n:
        result = str(n % base) + result
        n //= base
    return result

def is_match(word):
    six = 0
    for i in range(len(word)):
        c = word[i]
        if c == "6":
            six += 1
            if i != 0 and word[i-1] in "1357":
                return 0
            if i != len(word) - 1 and word[i+1] in "1357":
                return 0

    return six == 1

answer = 0

for i in range(int("10000", 8), int("100000", 8)):
    if is_match(toBase(i, 8)):
        answer += 1
    
print("№262:", answer)
