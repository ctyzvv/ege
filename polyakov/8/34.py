from itertools import product
cmb = list(product("AZNS", repeat=5))
print("№34:",
    cmb.index(("S", "A", "Z", "A", "N")) - 
    cmb.index(("Z", "A", "N", "A", "S")) + 1
    )
