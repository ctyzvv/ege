from itertools import product
cmb = list(product("CLON", repeat=5))
answer = 0
for i in cmb:
    if 1 <= i.count("O") <= 3:
        answer += 1
print("№54:", answer)
