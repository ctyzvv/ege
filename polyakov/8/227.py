from itertools import product

cmb = list(set(product("01234567", repeat=4)))

answer = 0

def is_match(word):
    if word[0] == "0":
        return 0

    number = int(word, 8)
    return not(number % 4)

for i in cmb:
    word = ""
    for j in i:
        word += j

    if is_match(word):
        answer += 1

print("№227:", answer)
