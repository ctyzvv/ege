from itertools import product

cmb = list(product("АПРСУ", repeat=5))

answer = 0

def is_match(word):
    if word[0] != "У":
        return 0
    if word.count("АА"):
        return 0
    return 1

for i in cmb:
    word = ""
    for j in i:
        word += j
    answer += 1
    if is_match(word):
        break

print("№249:", answer)
