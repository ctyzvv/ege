from functools import lru_cache

found = set()
symbols = "ГОЛ"

@lru_cache(None)
def recursion(word, lenght, symbols):
    if len(word) == lenght:
        if word in found:
            return 0
        found.add(word)
        return 1

    count = 0
        
    for i in symbols:
        if (word == "" or len(word) == 19) and i == "Г":
            continue

        if len(word) >= 2 and word[-1] == "Г" and word[-2] == "О" and i != "Л":
            continue

        if len(word) >= 2 and word[-1] == "Г" and word[-2] == "Л" and i != "О":
            continue
        
        if word != "" and word[-1] == i:
            continue

        count += recursion(word+i, lenght, symbols)
            
    return count


print("№247:", recursion("", 20, symbols))
