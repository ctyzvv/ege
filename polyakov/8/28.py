from itertools import product
cmb = list(product("KPANT", repeat=5))

answer = 0

for i in cmb:
    if i.count("K") == 2:
        answer += 1

print("№28:", answer)
