from itertools import product
cmb = list(set(product("САКУРА", repeat=5)))

answer = 0

for i in cmb:
    if i.count("А") <= 1 and i.count("У") <= 1:
        answer += 1

print("№188:", answer)
