from itertools import product
cmb = list(product("AOY", repeat=5))
print("№38:",
    cmb.index(("Y", "A", "Y", "A", "Y")) -
    cmb.index(("O", "Y", "O", "Y", "A")) + 1
    )
