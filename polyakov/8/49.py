from itertools import product
cmb = list(product("KATER", repeat=3))
answer = 0
for i in cmb:
    if i.count("R") >= 2:
        answer += 1
print("№49:", answer)
