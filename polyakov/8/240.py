from itertools import permutations

cmb = list(set(permutations("АКАРИДА")))

answer = 0


def is_match(word):
    for i in range(1, len(word)):
        if word[i-1] in "АИ" and word[i] in "АИ":
            return 0

        if word[i-1] in "КРД" and word[i] in "КРД":
            return 0
    return 1

for i in cmb:
    word = ""
    for j in i:
        word += j

    if is_match(word):
        answer += 1

print("240:", answer)

