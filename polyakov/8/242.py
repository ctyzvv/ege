from itertools import permutations

cmb = list(set(permutations("АТТЕСТАТ")))

answer = 0


def is_match(word):
    found = 0
    for i in range(1, len(word)):
        if word[i-1] in "АЕ" and word[i] in "АЕ":
            found = 1
            break

        if word[i-1] in "ТС" and word[i] in "ТС":
            found = 1
            break
    return found

for i in cmb:
    word = ""
    for j in i:
        word += j

    if is_match(word):
        answer += 1

print("242:", answer)

