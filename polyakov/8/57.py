from itertools import product
cmb = list(product("PIROG", repeat=6))
answer = 0

def is_match(word):
    if word.count("R") != 1:
        return 0

    i = word.index("R")
    if i == 0:
        return 0

    return word[i - 1] in "IO"
    

for i in cmb:
    word = i[0] + i[1] + i[2] + i[3] + i[4] + i[5]
    if is_match(word):
        answer += 1

print("№57:", answer)
