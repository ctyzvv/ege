from itertools import product
cmb = list(product("KLOYN", repeat=5))
answer = 0
for i in cmb:
    if i.count("Y") >= 1:
        answer += 1
print("№45:", answer)
