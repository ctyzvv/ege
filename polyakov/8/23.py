from itertools import product
cmb = list(product("KOT", repeat=6))

answer = 0

for i in cmb:
    if i.count("K") == 2:
        answer += 1

print("№23:", answer)
