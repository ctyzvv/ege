from itertools import product
cmb = list(product("MAPT", repeat=6))

answer = 0

for i in cmb:
    if i.count("P") == 2:
        answer += 1

print("№24:", answer)
