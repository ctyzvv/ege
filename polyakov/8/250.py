from itertools import product

cmb = list(product("ЕКЛОСТ", repeat=5))

answer = 0

def is_match(word):
    if word[0] != "С":
        return 0
    if word.count("ОО"):
        return 1
    return 0

for i in cmb:
    word = ""
    for j in i:
        word += j
    answer += 1
    if is_match(word):
        break

print("№250:", answer)
