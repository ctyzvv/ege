from itertools import product
cmb = list(set(product("КОРНЕТ", repeat=5)))

answer = 0

for i in cmb:
    if i.count("О") <= 1 and i.count("Е") <= 1:
        answer += 1

print("№189:", answer)
