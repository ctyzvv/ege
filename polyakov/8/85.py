from itertools import product
cmb = list(product("АОУ", repeat=5))

count = 0

for i in cmb:
    count += 1
    if i[2] == "У":
        break

print("№85:", count)


