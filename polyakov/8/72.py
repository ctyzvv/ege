from itertools import product
cmb = list(product("АКЛОШ", repeat=4))

count = 0

for i in cmb:
    count += 1
    if i[0] == "О":
        break

print("№72:", count)
