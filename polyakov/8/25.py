from itertools import product
cmb = list(product("TOK", repeat=6))

answer = 0

for i in cmb:
    if i[0] in "TK":
        answer += 1

print("№25:", answer)
