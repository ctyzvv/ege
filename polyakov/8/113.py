from itertools import permutations
cmb = list(set(permutations("ПЕСКАРЬ")))

answer = 0

def is_match(word):
    if word[1] == "Ь":
        return 0

    for i in range(len(word) - 1):
        if word[i] == "Ь" and word[i+1] in "ЕАР":
            return 0
    return 1

for i in cmb:
    word = ""
    for j in i:
        word += j

    if is_match(word):
        answer += 1

print("№113:", answer)
