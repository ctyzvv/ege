from itertools import product
cmb = list(product("ВЕКНО", repeat=5))

answer = 0

for i in range(len(cmb)):
    el = cmb[i]
    if el.count("О") == 1 and el.count("Е") == 1:
        answer = i + 1

print("№86:", answer)
