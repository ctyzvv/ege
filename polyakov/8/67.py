from itertools import product
from string import ascii_letters

# all combinations of lowercase letters
cmb = list(product(ascii_letters[0:26], repeat=3))

print("№67:", len(cmb))
