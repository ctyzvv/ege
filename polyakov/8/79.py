from itertools import product
cmb = list(product("АГОР", repeat=4))

count = 0
for i in cmb:
    count += 1
    if i.count("А") == 0:
        break
print("№79:", count)
