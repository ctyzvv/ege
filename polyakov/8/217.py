from itertools import product

cmb = []
cmb1 = list(set(product("ЧОАНИМЕ", repeat=4)))
cmb2 = list(set(product("ЧОАНИМЕ", repeat=5)))
cmb3 = list(set(product("ЧОАНИМЕ", repeat=6)))

cmb = cmb1 + cmb2 + cmb3

answer = 0

def is_match(word): 
    
    if word.count("М") != 3:
        return 0
    return 1

for i in cmb:
    word = ""
    for j in i:
        word += j

    if is_match(word):
        answer += 1

print("217:", answer)
