from itertools import product
cmb = list(product("АКЛОШ", repeat=5))

answer = 0

def is_match(word):
    for i in word:
        if i in "АО":
            return 1
    return 0

for i in cmb:
    word = ""
    for j in i:
        word += j

    if is_match(word):
        answer += 1

print("№178:", answer)
