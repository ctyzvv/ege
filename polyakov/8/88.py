from itertools import product

alphabet = "AA"
answer = 0
count = 0
while count != 486:
    cmb = list(product(alphabet, repeat=6))
    count = 0

    for i in cmb:
        if i[0] == "A":
            count += 1

    if count != 486:
        alphabet += "B"
        answer += 1

print("№88:", answer)

    
    
