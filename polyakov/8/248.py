from functools import lru_cache
symbols = "CONST"

found = set()

@lru_cache(None)
def recursion(cur, prev, symbols, need):
    if need == 0:
        return cur != "S"
    
    count = 0
    for i in symbols:
        if i == cur:
            continue
        if cur == "S" and i == prev:
            continue

        count += recursion(i, cur, symbols, need-1)
    return count

print("№248:", recursion("S", " ", symbols, 16))
