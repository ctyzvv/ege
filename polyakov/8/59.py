from itertools import product
cmb = list(product("PIROG", repeat=5))
answer = 0

def is_match(word):
    if word.count("R") > 2:
        return 0
    
    if word.count("R") == 0:
        return 1

    for i in range(len(word)):
        if word[i] == "R":
            if i == len(word) - 1:
                return 0 
            if not (word[i + 1] in "IO"):
                return 0
        
    return 1

for i in cmb:
    word = i[0] + i[1] + i[2] + i[3] + i[4]
    if is_match(word):
        answer += 1

print("№59:", answer)
