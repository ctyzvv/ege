from itertools import product
cmb = list(product("LETO", repeat=4))
answer = 0
for i in cmb:
    if i.count("E") >= 1:
        answer += 1
print("№42:", answer)
