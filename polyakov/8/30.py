from itertools import product
cmb = list(product("GOD", repeat=6))
answer = 0
for i in cmb:
    if i[0] in "GD" and i[-1] in "GD":
        answer += 1
print("№30:", answer)
