from itertools import permutations
cmb = list(permutations("МАНОК"))

answer = 0

for i in cmb:
    word = ""
    for j in i:
        word += j

    if word[0] != "О" and not ("АО" in word):
        answer += 1

print("№92:", answer)

