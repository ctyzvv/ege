from itertools import product
cmb = list(product("РЕЖИМДНО", repeat=6))

answer = 0

def is_match(word):
    if not (word[0] in "РЖМДН") or not (word[1] in "ЕИО"):
        return 0
    if not (word[-1] in "ЕИО"):
        return 0

    for i in word:
        if word.count(i) > 1:
            return 0
    return 1

for i in cmb:
    word = ""
    for j in i:
        word += j

    if is_match(word):
        answer += 1

print("№207:", answer)
