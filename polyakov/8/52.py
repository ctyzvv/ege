from itertools import product
cmb = list(product("KOMAR", repeat=4))
answer = 0
for i in cmb:
    if i.count("A") <= 3:
        answer += 1
print("№52:", answer)
