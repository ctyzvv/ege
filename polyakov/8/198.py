from itertools import product
cmb = list(product("АБВГД", repeat=3))

answer = 0

for i in cmb:
    if list(i) == sorted(i):
        answer += 1

print("№198:", answer)
