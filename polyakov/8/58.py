from itertools import product
cmb = list(product("PIROG", repeat=4))
answer = 0

def is_match(word):
    if word.count("O") > 2:
        return 0
    
    if word.count("O") == 0:
        return 1

    for i in range(len(word)):
        if word[i] == "O":
            if i == 0:
                return 0 
            if word[i - 1] in "IO":
                return 0
        
    return 1

for i in cmb:
    word = i[0] + i[1] + i[2] + i[3] 
    if is_match(word):
        answer += 1

print("№58:", answer)
