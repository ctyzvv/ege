from itertools import product
cmb = list(product("BALKON", repeat=4))
answer = 0
for i in cmb:
    if i.count("B") >= 1:
        answer += 1
print("№47:", answer)
