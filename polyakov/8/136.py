from itertools import product
cmb = list(set(product("ГЕРОЙ", repeat=4)))
answer = 0

def is_match(word):
    
    if word[0] == "Й":
        return 0
    
    for i in word:
        if i in "ЕО":
            return 1
    return 0

for i in cmb:
    word = ""
    for j in i:
        word += j
    if is_match(word):
        answer += 1

print("№136:", answer)

