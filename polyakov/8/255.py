numbers = "0123456789ABCDEF"
def toBase(n, base):
    result = ""
    while n:
        result = str(numbers[n%base]) + result
        n //= base
    return result

def is_match(word):
    
    if word.count("65") + word.count("56") != 1:
        return 0
    return 1

answer = 0

for i in range(int("70000", 8), int("77777", 8), 2):
    if is_match(toBase(i, 8)):
        answer += 1

print("№255:", answer)
