from itertools import product
cmb = list(product("KYMA", repeat=5))

answer = 0

for i in cmb:
    if i[0] in "KM" and i[-1] in "YA":
        answer += 1

print("№26:", answer)
