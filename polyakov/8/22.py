from itertools import product
cmb = list(product("KOT", repeat=5))

answer = 0

for i in cmb:
    if i.count("O") == 2:
        answer += 1

print("№22:", answer)
