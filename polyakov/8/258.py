from itertools import product

cmb = list("".join(word) for word in product("АИКЛМЬ", repeat=6))

answer = 0

def is_match(word):
    if not (word[0] == "К" and word[-1] == "Ь"):
        return 0
    for i in word:
        if word.count(i) != 1:
            return 0
    return 1

for i in range(len(cmb)):
    index1 = cmb.index((cmb[i])[::-1])
    index2 = i

    if is_match(cmb[i]) and abs(index1 - index2) == 26655:
        for j in str(index2 + 1):
            answer += int(j)
        break


print("№258:", answer)
