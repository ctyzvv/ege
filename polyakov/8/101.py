from itertools import permutations
cmb = list(permutations("ШАНЕЛЬ"))

answer = 0

for i in cmb:
    word = ""
    for j in i:
        word += j

    if word[0] != "Ь" and not("ЕАЬ" in word):
        answer += 1

print("№101:", answer)
