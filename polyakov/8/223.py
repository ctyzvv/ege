from itertools import permutations

cmb = list(set(permutations("ОДЕКОЛОН")))

answer = 0

def is_match(word):
    if word.count("ОО"):
        return 0
    return 1

for i in cmb:
    word = ""
    for j in i:
        word += j

    if is_match(word):
        answer += 1

print("№223:", answer)
