from itertools import permutations

cmb = list(set(permutations("ШАРЛАТАН")))

answer = 0


def is_match(word):
    found = 0
    for i in range(1, len(word)):
        if word[i-1] in "А" and word[i] in "А":
            found = 1
            break

        if word[i-1] in "ШРЛТН" and word[i] in "ШРЛТН":
            found = 1
            break
    return found

for i in cmb:
    word = ""
    for j in i:
        word += j

    if is_match(word):
        answer += 1

print("244:", answer)

