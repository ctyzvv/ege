from itertools import product

cmb = list(product("АЕПСТУХ", repeat=4))

answer = 0

def is_match(word):
    for i in range(1, len(word)):
        if word[i-1] == word[i]:
            return 0
    return 1

for i in range(1000, len(cmb)):
    if is_match(cmb[i]):
        answer += 1

print("№257:", answer)
