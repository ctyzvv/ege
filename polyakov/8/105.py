from itertools import permutations
cmb = list(permutations("НОБЕЛИЙ"))

answer = 0

for i in cmb:
    word = ""
    for j in i:
        word += j

    if word[0] != "Й" and not("ИЙО" in word):
        answer += 1

print("№105:", answer)
