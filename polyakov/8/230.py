from itertools import product

cmb = list(set(product("012345678", repeat=7)))

answer = 0

def is_match(word):
    if word[0] in "037":
        return 0

    for i in range(1, len(word)):
        if word[i] == word[i-1]:
            return 0

    return 1

for i in cmb:
    word = ""
    for j in i:
        word += j

    if is_match(word):
        answer += 1

print("№230:", answer)
