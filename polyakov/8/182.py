from itertools import product
cmb = list(product("АВДПР", repeat=4))

answer = 0

def is_match(word):
    if word.count("А") != 0:
        return 0
    for i in word:
        if word.count(i) != 1:
            return 0
    return 1
            

for i in range(len(cmb)):
    word = ""
    for j in cmb[i]:
        word += j

    if is_match(word):
        answer = i + 1
        break

print("№182:", answer)
