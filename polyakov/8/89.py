from itertools import product

alphabet = "AAG"
answer = 1
count = 0
while count != 512:
    cmb = list(product(alphabet, repeat=6))
    count = 0

    for i in cmb:
        if i[0] == "A" and i[-1] == "G":
            count += 1

    if count != 512:
        alphabet += "B"
        answer += 1

print("№89:", answer)

    
    
