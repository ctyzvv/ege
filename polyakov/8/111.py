from itertools import permutations
cmb = list(set(permutations("АЙСБЕРГ")))

answer = 0

def is_match(word):
    if word[0] == "Й":
        return 0

    for i in range(len(word) - 1):
        if word[i] == "Й" and word[i+1] in "АЕ":
            return 0
    return 1

for i in cmb:
    word = ""
    for j in i:
        word += j

    if is_match(word):
        answer += 1

print("№111:", answer)
