from itertools import product

cmb = list(set(product("0123456", repeat=7)))

answer = 0

def is_match(word):
    if word[0] in "035":
        return 0

    if word.count("22") and word.count("44"):
        return 0

    return 1

for i in cmb:
    word = ""
    for j in i:
        word += j

    if is_match(word):
        answer += 1

print("№229:", answer)
