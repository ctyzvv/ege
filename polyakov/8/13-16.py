from itertools import product
cmb = list(product("AKPY", repeat=5))

print("№13: ", cmb.index(("Y", "A", "A", "A", "A")) + 1)
print("№14: ", cmb.index(("K", "A", "A", "A", "A")) + 1)
print("№15: ", cmb.index(("P", "Y", "K", "A", "A")) + 1)
print("№16: ", cmb.index(("Y", "K", "A", "P", "A")) + 1)


