from itertools import product
cmb = list(product("АБВГДЕ", repeat=4))

def is_match(word):
    if word.count("Г") != 1:
        return 0 

    return word[0] == "Г" or word[-1] == "Г"

answer = 0

for i in cmb:

    word = ""
    for j in i:
        word += j

    if is_match(word):
        answer += 1

print("№69:", answer)
