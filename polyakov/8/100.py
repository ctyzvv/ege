from itertools import permutations
cmb = list(permutations("ПАНЕЛЬ"))

answer = 0

for i in cmb:
    word = ""
    for j in i:
        word += j

    if word[0] != "Ь" and not ("ЕАП" in word):
        answer += 1

print("№100:", answer)

