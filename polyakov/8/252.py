from itertools import product

cmb = list(set(product("ЕГЭИНФ", repeat=6)))

answer = 0

def is_match(word):
    if word[0] != "Е":
        return 0

    if word.count("ФИ") < 2:
        return 0
    
    if word.count("ЕГЭ"):
        return 0

    if not(word[-1] in "ЭИ"):
        return 0

    return 1

for i in cmb:
    word = ""
    for j in i:
        word += j
    if is_match(word):
        answer += 1

print("№252:", answer)
