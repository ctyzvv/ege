from itertools import product
cmb = list(product("ABCD", repeat=3))

answer = 0

def is_match(word):
    if word.count("BC"):
        return 0

    if word.count("CB"):
        return 0

    length = len(word)
    for i in range(length):
        if word[i] == "A":
            if i > 0 and word[i-1] == "D":
                continue
            if i < length - 1 and word[i+1] == "D":
                continue
            return 0

    return 1

for i in cmb:
    word = i[0] + i[1] + i[2]
    if is_match(word):
        answer += 1

print("№32:", answer)
