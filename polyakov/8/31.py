from itertools import product
cmb = list(product("METPO", repeat=4))
answer = 0
for i in cmb:
    if i[0] in "MTP" and i[-1] in "EO":
        answer += 1
print("№31:", answer)
