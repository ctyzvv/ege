from itertools import product
cmb = list(product("АБВГ", repeat=5))

def is_match(word):
    if word.count("Г") > 1:
        return 0 

    if word.count("Г") == 0:
        return 1

    return word[-1] == "Г"

answer = 0

for i in cmb:

    word = ""
    for j in i:
        word += j

    if is_match(word):
        answer += 1

print("№70:", answer)
