from itertools import product
cmb = list(product("AMRT", repeat=4))
print("№36:", 
    cmb.index(("R", "A", "M", "T")) -
    cmb.index(("M", "A", "R", "T")) + 1
    )
