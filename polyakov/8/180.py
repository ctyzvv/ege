from itertools import product
cmb = list(product("МЕЧТА", repeat=6))

answer = 0

def is_match(word):
    if word.count("А") >= 3:
        return 1
    return 0

for i in cmb:
    word = ""
    for j in i:
        word += j
    if is_match(word):
        answer += 1
    
print("№180:", answer)
