from itertools import product
cmb = list(product("ПИТОН", repeat=4))

answer = 0

def is_match(word):
    for i in range(1, len(word)):
        if word[i - 1] in "ПТН" and word[i] in "ПТН":
            return 0
        if word[i - 1] in "ИО" and word[i] in "ИО":
            return 0
    return 1

for i in cmb:
    word = ""
    for j in i:
        word += j

    if is_match(word):
        answer += 1

print("№202:", answer)
