
odd  = "13579"
even = "02468"

answer = 0

def is_match(n):
    number = str(n)[2:]
    found = number[0]
    for i in range(1, len(number)):
        if number[i] in odd and number[i - 1] in odd:
            return 0
        if number[i] in even and number[i - 1] in even:
            return 0
        if number[i] in found:
            return 0
        else:
            found += number[i]
    return 1

# 32768 - 0o100000,  262143 - 0o777777
for i in range(32768, 262143):
    if is_match(oct(i)):
        answer += 1

print("№161:", answer)
