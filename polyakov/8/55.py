from itertools import product
cmb = list(product("JIRAF", repeat=6))
answer = 0
for i in cmb:
    if 1 <= i.count("A") <= 4:
        answer += 1
print("№55:", answer)
