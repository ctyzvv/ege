from itertools import permutations
cmb = list(set(permutations("РУЛЬКА")))

answer = 0

def is_match(word):
    if word[0] == "Ь":
        return 0

    for i in range(1, len(word)):
        if word[i] == "Ь" and word[i-1] in "УА":
            return 0
    return 1

for i in cmb:
    word = ""
    for j in i:
        word += j

    if is_match(word):
        answer += 1

print("№110:", answer)
