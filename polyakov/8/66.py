from itertools import product
from string import ascii_letters

# all combinations of lowercase letters
cmb = list(product(ascii_letters[0:26], repeat=4))

answer = 0
for i in cmb:
    if i[:2] == i[:1:-1]:
        answer += 1

print("№66:", answer)
