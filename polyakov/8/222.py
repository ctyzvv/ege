from itertools import product

cmb = list(set(product("ЗЕРКАЛО", repeat=6)))

answer = 0

def is_match(word): 
    
    found = 0
    for i in word:
        if i == "К":
            found += 1
        elif word.count(i) > 1:
            return 0

    return 0 < found <= 4

for i in cmb:
    word = ""
    for j in i:
        word += j

    if is_match(word):
        answer += 1

print("222:", answer)
