from itertools import product
cmb = list(product("ВЕКНО", repeat=5))

answer = 0

for i in range(len(cmb)):
    el = cmb[i]
    if el.count("Н") == 2 and el.count("К") == 2:
        answer = i + 1

print("№87:", answer)
