from itertools import product
cmb = list(product("АБВГДЭЮЯ", repeat=5))
answer = 0

def is_match(word):
    
    if not (word[0] in "ЭЮЯ"):
        return 0
    
    if not (word[-1] in "ЭЮЯ"):
        return 0

    for i in range(1, len(word) - 1, 1):
        if word[i] in "ЭЮЯ":
            return 0

    return 1


for i in cmb:
    word = i[0] + i[1] + i[2] + i[3] + i[4]
    if is_match(word):
        answer += 1

print("№61:", answer)
