from itertools import product
cmb = list(product("АРТФ", repeat=5))

count = 0

for i in cmb:
    count += 1
    if i[0] == "Т":
        break

print("№73:", count)
