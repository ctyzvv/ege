from itertools import permutations
cmb = list(permutations("КУПЧИХА"))

answer = 0

for i in cmb:
    word = ""
    for j in i:
        word += j

    if word[0] != "Ч" and not("ИАУ" in word):
        answer += 1

print("№103:", answer)
