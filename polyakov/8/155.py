from itertools import product
cmb = list(product("АИОУЭ", repeat=6))

answer = 0

for i in range(len(cmb)):
    el = cmb[i]
    if el[0] == "О" and el[-1] == "О":
        answer = i + 1

print("№155:", answer)
