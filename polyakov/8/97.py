from itertools import permutations
cmb = list(permutations("КАЛИЙ"))

answer = 0

for i in cmb:
    word = ""
    for j in i:
        word += j

    if word[0] != "Й" and not ("ИАК" in word):
        answer += 1

print("№97:", answer)

