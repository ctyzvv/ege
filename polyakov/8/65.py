from itertools import product
cmb = list(product("АБВГДЯ", repeat=5))
answer = 0

def is_match(word):
    if word.count("Я") != 1:
        return 0


    return (word[0] == "Я" or word[-1] == "Я")
        

for i in cmb:
    word = i[0] + i[1] + i[2] + i[3] + i[4]
    if is_match(word):
        answer += 1

print("№65:", answer)
