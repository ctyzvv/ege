from itertools import product
cmb = list(product("ДЕЙКСТРА", repeat=6))

answer = 0

def is_match(word):
    if word.count("Й") != 1:
        return 0

    ind = word.index("Й")

    if ind == len(word) - 1:
        return 0

    if not (word[ind + 1] in "ДКСТР"):
        return 0

    for i in word:
        if word.count(i) > 1:
            return 0

    return 1
for i in cmb:
    word = ""
    for j in i:
        word += j

    if is_match(word):
        answer += 1

print("№208:", answer)
