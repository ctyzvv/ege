from itertools import product

phrase = "ВОЛК"
cmb = list("".join(word) for word in product("ПОЛЯКВ", repeat=4))

answer = 0

def is_match(word):
    found = 0
    for i in range(len(word)):
        if word[i] == phrase[i]:
            found += 1

        
    return found == 2


for i in cmb:
    if is_match(i):
        answer += 1


print("№259:", answer)
