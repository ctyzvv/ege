from itertools import product
cmb = list(product("0123456789", repeat=6))

answer = 0

def is_match(word):
    if word[0] == "0":
        return 0
    for i in range(1, len(word)):
        if word[i - 1] in "02468" and word[i] in "02468":
            return 0
        if word[i - 1] in "13579" and word[i] in "13579":
            return 0
        if word.count(word[i]) > 1:
            return 0
    return 1

for i in cmb:
    word = ""
    for j in i:
        word += j

    if is_match(word):
        answer += 1

print("№204:", answer)
