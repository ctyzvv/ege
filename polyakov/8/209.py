from itertools import product
cmb = list(product("АПЕЛЬСИН", repeat=7))

answer = 0

def is_match(word):

    found = 0
    for i in word:
        if i == "Ь":
            found = 1

        if word.count(i) > 1:
            return 0

    if found:
        ind = word.index("Ь")
        if ind == 0 or ind == len(word) - 1:
            return 0
        if word[ind - 1] in "ПЛСН" and word[ind + 1] in "ПЛСН":
            return 1
        return 0

    return 1
for i in cmb:
    word = ""
    for j in i:
        word += j

    if is_match(word):
        answer += 1

print("№209:", answer)
