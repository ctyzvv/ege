from itertools import product
cmb = list(set(product("ТАРАКАНИЩЕ", repeat=6)))

answer = 0

def is_match(word):
    if not (word[0] in "ТРКНЩ"):
        return 0

    for i in range(1, len(word)):
        if word[i - 1] in "ТРКНЩ" and word[i] in "ТРКНЩ":
            return 0

        if word[i - 1] in "АИЕ" and word[i] in "АИЕ":
            return 0

        if word.count(word[i]) > 1:
            return 0
            
    return 1


for i in cmb:
    word = ""
    for j in i:
        word += j

    if is_match(word):
        answer += 1

print("№77:", answer)
