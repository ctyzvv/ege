from itertools import product
cmb = list(product("ЛОГАРИФМ", repeat=5))

answer = 0

def is_match(word):
    for i in range(1, len(word)):
        if word[i - 1] in "ЛГРФМ" and word[i] in "ЛГРФМ":
            return 0
        if word[i - 1] in "АИО" and word[i] in "АИО":
            return 0
        if word.count(word[i]) > 1:
            return 0
    return 1

for i in cmb:
    word = ""
    for j in i:
        word += j

    if is_match(word):
        answer += 1

print("№203:", answer)
