def toBase(n, base):
    result = ""
    while n:
        result = str(n % base) + result
        n //= base
    return result

def is_match(word):
    if word[0] in "1357":
        return 0
    if word[-1] in "18":
        return 0
    return word.count("3") <= 1

answer = 0

for i in range(int("10000", 9), int("100000", 9)):
    if is_match(toBase(i, 9)):
        answer += 1
    
print("№263:", answer)
