from itertools import permutations

cmb = list(set(permutations("МАРИНА")))

answer = 0

def is_match(word):
    if word[0] in "АИ":
        return 0

    return 1

for i in cmb:
    word = ""
    for j in i:
        word += j

    if is_match(word):
        answer += 1

print("№228:", answer)
