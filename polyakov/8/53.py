from itertools import product
cmb = list(product("MYXA", repeat=5))
answer = 0
for i in cmb:
    if i.count("Y") <= 3:
        answer += 1
print("№53:", answer)
