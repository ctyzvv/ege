from itertools import product

cmb = list("".join(word) for word in product("ГЕКЭ023", repeat=4))

def is_match(word):
    if not (word[0] in "0123"):
        return 0

    for i in range(1, len(word)):
        if word[i-1] == word[i]:
            return 0
    
    return 1

answer = 0

for i in cmb:
    answer += 1
    if is_match(i):
        break

print("№264:", answer)
