from itertools import product

cmb = list(set(product("012345678", repeat=7)))

answer = 0

def is_match(word):
    if word[0] in "0246":
        return 0

    if word[-1] == word[-2] == word[-3]:
        return 0
    return 1

for i in cmb:
    word = ""
    for j in i:
        word += j

    if is_match(word):
        answer += 1

print("№233:", answer)
