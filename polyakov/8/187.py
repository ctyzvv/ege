from itertools import product
cmb = list(product("БАНКИР", repeat=6))

answer = 0

for i in cmb:
    if i.count("А") <= 1 and i.count("И") <= 1:
        answer += 1

print("№187:", answer)
