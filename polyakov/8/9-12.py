from itertools import product
cmb = list(product("АОУ", repeat=5))

print("№9 :", cmb.index(("У", "А", "А", "А", "А")) + 1)
print("№10:", cmb.index(("О", "А", "О", "А", "О")) + 1)
print("№11:", cmb.index(("У", "А", "У", "А", "У")) + 1)
print("№12:", cmb.index(("О", "А", "А", "А", "А")) + 1)
