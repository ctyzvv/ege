from itertools import product

cmb = list(set(product("01234567", repeat=4)))

answer = 0

def is_match(word):
    if not (word[0] in "246"):
        return 0

    for i in range(1, len(word)):
        if word[i] > word[i-1]:
            return 0
    return 1

for i in cmb:
    word = ""
    for j in i:
        word += j

    if is_match(word):
        answer += 1

print("№224:", answer)
