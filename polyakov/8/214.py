from itertools import product

cmb = list(set(product("ВАЙФУ", repeat=4)))

answer = 0

def is_match(word): 
    
    if word[0] == "Й":
        return 0

    for i in range(1, len(word)):
        if word[i] == "Ф" and word[i - 1] == "В":
            return 0

        if word[i] == "В" and word[i - 1] == "Ф":
            return 0

        if word.count(word[i]) > 1:
            return 0
    
    return 1

for i in cmb:
    word = ""
    for j in i:
        word += j

    if is_match(word):
        answer += 1

print("214:", answer)
