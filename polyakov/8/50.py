from itertools import product
cmb = list(product("KATER", repeat=4))
answer = 0
for i in cmb:
    if i.count("R") >= 2:
        answer += 1
print("№50:", answer)
