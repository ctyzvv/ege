from itertools import permutations

cmb = list(set(permutations("СПОРТЛОТО")))


answer = 0

for i in range(len(cmb)):
    word = cmb[i]
    if word[0] != "О" and word[-1] != "О": 
        answer +=  1

print("№265:", answer)
