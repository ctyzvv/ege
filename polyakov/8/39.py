from itertools import product
cmb = list(product("ABCDX", repeat=4))
answer = 0
for i in cmb:
    if i.count("X") == 1:
        answer += 1
print("№39:", answer)
