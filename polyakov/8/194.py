from itertools import product
cmb = list(product("ЛИДА", repeat=3)) + list(product("ЛИДА", repeat=4)) + list(product("ЛИДА", repeat=5))

answer = 0

def is_match(word):
    if word.count("И") > 2:
        return 0
    if word.count("А") > 2:
        return 0

    
    for i in range(1, len(word)):
        if word[i] in "ЛД":
            return 0

    return 1

for i in cmb:
    word = ""
    for j in i:
        word += j

    if is_match(word):
        answer += 1

print("№194:", answer)
